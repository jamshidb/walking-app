package com.jamshid.letswalk;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.text.InputType;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class LetsWalkActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        PopupMenu.OnMenuItemClickListener {

    private static final String TAG = LetsWalkActivity.class.getSimpleName();
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    private Marker mCurrLocationMarker;

    PolylineOptions polylineOptions;
    PolylineOptions toAdd;
    PolylineOptions retrievedFromDB;

    int count;
    boolean stop;
    boolean track;
    boolean check = true;
    boolean info = true;
    boolean original = true;
    boolean startUp = true;
    double latitude;
    double longitude;
    private String userInput="";
    Route route;
    String retrievedMedia;
    String retrievedRoute;
    String retrievedUser;
    String mediaLocation="";
    String nameUID;
    String[] userInfo = new String[2];
    private int PROXIMITY_RADIUS = 1000;
    private String mCurrentPhotoPath;
    String imgName = "";
    private static final int REQUEST_IMAGE_CAPTURE = 111;
    final long TEN_MEGABYTE = 1024*1024*10;
    File imageFile;
    File photoFile;
    private ArrayList<Marker> markers = new ArrayList<Marker>();
    private ArrayList<Marker> imageMarkers = new ArrayList<Marker>();
    private StorageReference mStorageRef;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseDatabase database;

    private Button menuButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lets_walk);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mAuth = FirebaseAuth.getInstance();

        Toast.makeText(this,"Welcome "+mAuth.getCurrentUser().getDisplayName(), Toast.LENGTH_LONG).show();


        stop = false;
        track = false;
        polylineOptions = new PolylineOptions();
        toAdd = new PolylineOptions();
        retrievedFromDB = new PolylineOptions();
        polylineOptions.color(Color.MAGENTA);

        //Obtain the FirebaseAnalytics instance
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mStorageRef = FirebaseStorage.getInstance().getReference();


        menuButton = (Button) findViewById(R.id.menu);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(LetsWalkActivity.this, view);
                MenuInflater inflater = popupMenu.getMenuInflater();
                inflater.inflate(R.menu.menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(LetsWalkActivity.this);
                popupMenu.show();
            }
        });

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user != null) {
                    //User is signed in
                    mFirebaseAnalytics.logEvent("User_signed_in", new Bundle());
                    Log.d(TAG, "onAuthStateChanged:signed_in: "+user.getUid());
                } else {
                    //User is signed out
                    mFirebaseAnalytics.logEvent("User_signed_out", new Bundle());
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.StartRoute: //Start drawing
                if(track) {
                    Toast.makeText(this,"You have already started a walk, please end it before starting another by saving it.", Toast.LENGTH_LONG).show();
                } else {
                    track = true;
                }
                return true;

            case R.id.RetrieveRoute:
                retrieveRoute();
                return true;
            case R.id.SaveRoute:
                if(track) {
                    saveRoute();
                } else {
                    Toast.makeText(this,"You have not started a walk, there is nothing to save.", Toast.LENGTH_LONG).show();
                }
                return true;
            case R.id.restaurant:
                if(markers.size() > 0) {
                    clearMarkers();
                }
                String urlRes = getUrl(latitude, longitude, "restaurant");
                Object[] DataTransferRes = new Object[2];
                DataTransferRes[0] = mMap;
                DataTransferRes[1] = urlRes;
                Log.d("onClick", urlRes);
                GetNearbyPlacesData getNearbyPlacesDataRes = new GetNearbyPlacesData();
                getNearbyPlacesDataRes.execute(DataTransferRes);
                markers = getNearbyPlacesDataRes.markers;
                Toast.makeText(LetsWalkActivity.this,"Nearby Restaurants", Toast.LENGTH_LONG).show();
                return true;
            case R.id.school:
                if(markers.size() > 0) {
                    clearMarkers();
                }
                String urlSchool = getUrl(latitude, longitude, "park");
                Object[] DataTransferSchool = new Object[2];
                DataTransferSchool[0] = mMap;
                DataTransferSchool[1] = urlSchool;
                Log.d("onClick", urlSchool);
                GetNearbyPlacesData getNearbyPlacesDataSchool = new GetNearbyPlacesData();
                getNearbyPlacesDataSchool.execute(DataTransferSchool);
                markers = getNearbyPlacesDataSchool.markers;
                Toast.makeText(LetsWalkActivity.this,"Nearby Activities", Toast.LENGTH_LONG).show();
                return true;
            case R.id.hospital:
                if(markers.size() > 0) {
                    clearMarkers();
                }
                String url = getUrl(latitude, longitude, "museum");
                Object[] DataTransfer = new Object[2];
                DataTransfer[0] = mMap;
                DataTransfer[1] = url;
                Log.d("onClick", url);
                GetNearbyPlacesData getNearbyPlacesData = new GetNearbyPlacesData();
                getNearbyPlacesData.execute(DataTransfer);
                markers = getNearbyPlacesData.markers;
                Toast.makeText(LetsWalkActivity.this,"Nearby Museums", Toast.LENGTH_LONG).show();
                return true;
            case R.id.media:
                if(track) {
                    onLaunchCamera();
                } else {
                    Toast.makeText(LetsWalkActivity.this,"You can't capture a memory without starting a walk.", Toast.LENGTH_LONG).show();
                }

                    return true;
            case R.id.logout : mAuth.signOut(); finish();
            default : return super.onOptionsItemSelected(item);
        }
    }

    private void clearMarkers() {
        for(Marker marker : markers) {
            marker.remove();
        }
    }

    private void onLaunchCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create the file where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch(IOException e) {
                //Error while creating file
            }

            //Continue only if the file was successfully created
            if(photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(this,"com.jamshid.letswalk",photoFile);
                Log.d(TAG, "ENTERING PHOTO APP");
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }


        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //We handle the activity carried out i.e. picture taken
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            //Photo is at location mCurrentPhotoPath

            File file = new File(mCurrentPhotoPath);
            Uri uri = Uri.fromFile(file);
            Bitmap imageBitmap;
            try {
                imageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                encodeBitmapAndSaveToFirebase(imageBitmap);
                //imageBitmap = crupAndScale(imageBitmap, 300);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static Bitmap crupAndScale (Bitmap source, int scale) {
        int factor = source.getHeight() <= source.getWidth() ? source.getHeight(): source.getWidth();
        int longer = source.getHeight() >= source.getWidth() ? source.getHeight(): source.getWidth();
        int x = source.getHeight() >= source.getWidth() ?0:(longer-factor)/2;
        int y = source.getHeight() <= source.getWidth() ?0:(longer-factor)/2;
        source = Bitmap.createBitmap(source, x, y, factor, factor);
        source = Bitmap.createScaledBitmap(source, scale, scale, false);
        return source;
    }

    private void encodeBitmapAndSaveToFirebase(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] data = baos.toByteArray();

        saveImage(data, generateRandomWord());
    }


    private void saveImage(byte[] data, final String imageName) {
        //Create a reference to images in our storage
        StorageReference imageRef = mStorageRef.child("images").child(mAuth.getCurrentUser().getDisplayName()).child(mAuth.getCurrentUser().getUid()).child(imageName+".jpg");
        Log.d("SAVING IMAGE", imageRef.toString());
        mediaLocation = mediaLocation+imageName+","+latitude+","+longitude+"|";
        imgName  = imgName.concat(mediaLocation);
        MarkerOptions photoMarker = new MarkerOptions();
        photoMarker.position(new LatLng(latitude, longitude));
        photoMarker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
        Marker marker = mMap.addMarker(photoMarker);
        marker.setTag(mAuth.getCurrentUser().getDisplayName()+","+mAuth.getCurrentUser().getUid()+","+imageName);
        //marker

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if(imageMarkers.contains(marker)) {
                    String ref = (String) marker.getTag();
                    String[] values = ref.split(",");
                    StorageReference retrieveRef = mStorageRef.child("images").child(values[0]).child(values[1]).child(values[2]+".jpg");
                    Log.d(TAG, "retrieveRef: "+retrieveRef.toString());

                    photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch(IOException e) {
                        //Error while creating file
                    }
                    //Continue only if the file was successfully created
                    if(photoFile != null) {
                        retrieveRef.getFile(photoFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                Intent i = new Intent();
                                i.setAction(Intent.ACTION_VIEW);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                i.setDataAndType(FileProvider.getUriForFile(LetsWalkActivity.this, "com.jamshid.letswalk", photoFile), "image/jpg");
                                startActivity(i);
                            }
                        });
                    }

                }
                return true;
            }
        });
        imageMarkers.add(marker);

        UploadTask uploadTask = imageRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                //taskSnapshot.getmatadata() contrains faile metadata such as size, content-type and download URL.
                //Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Toast.makeText(LetsWalkActivity.this,"Photo has been saved.", Toast.LENGTH_LONG).show();
                Log.d(TAG, "PHOTO SAVED SUCCESSFULLY");
                Log.d(TAG, "LOCATION: "+mediaLocation);

            }
        });
    }

    public static String generateRandomWord()
    {
        Random random = new Random();
        char[] word = new char[random.nextInt(8)+3]; // words of length 3 through 10. (1 and 2 letter words are boring.)
        for(int j = 0; j < word.length; j++){
                word[j] = (char)('a' + random.nextInt(26));
        }
        return new String(word);
    }

    private File createImageFile() throws IOException {
        //Create an image file name
        String timeStamp = new SimpleDateFormat("yyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_"+timeStamp+"_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName, //prefix
                ".jpg",        //suffix
                storageDir     //directory
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private String getUrl(double latitude, double longitude, String nearbyPlace) {

        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&type=" + nearbyPlace);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + "AIzaSyATuUiZUkEc_UgHuqsBJa1oqaODI-3mLs0");
        Log.d("getUrl", googlePlacesUrl.toString());
        return (googlePlacesUrl.toString());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    public void onStop() {
        super.onStop();
        if(mAuth != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        Log.d(TAG, "LOCATION: "+location.toString());
        mLastLocation = location;

        //Place current location marker
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Start Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        if (mCurrLocationMarker == null) {
            if(startUp) {
                startUp = false;
                //move map camera
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomIn());
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
            } else if(track){
                mCurrLocationMarker = mMap.addMarker(markerOptions);
            }
        }

        if(count < 3) {
            count++;
            return;
        }
        if(!track) {return;}

        //Draw path
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);
        polylineOptions.add(latLng);

        if(polylineOptions.getPoints().size() < 2) {
            //don't draw
        } else {
            mMap.addPolyline(polylineOptions);
        }

        //stop location updates
        if (mGoogleApiClient != null && stop) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }


    private void getInput(boolean flag) {
        //Have a flag for when we're saving data and retrieving data
        final boolean Flag = flag;
        database = FirebaseDatabase.getInstance();
        final DatabaseReference routeRef = database.getReference("Route Data");

        AlertDialog.Builder builder = new AlertDialog.Builder(LetsWalkActivity.this);
        builder.setTitle("Enter route name: ");

        // Set up the input
        final EditText input = new EditText(LetsWalkActivity.this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                userInput = input.getText().toString();
                final DatabaseReference dataRef =  routeRef.child(mAuth.getCurrentUser().getDisplayName()).child(mAuth.getCurrentUser().getUid()).child(userInput);
                if(Flag) {
                    //We are saving to the database
                    //Before we save, check if there is something there already.
                   dataRef.addListenerForSingleValueEvent(new ValueEventListener() {
                       @Override
                       public void onDataChange(DataSnapshot dataSnapshot) {
                           if (!dataSnapshot.hasChildren()) {
                               // doesn't Exist
                               mMap.clear();
                               toAdd = polylineOptions;
                               //polylineOptions = new PolylineOptions();
                               stop = true;
                               track = false;


                               route = new Route(toAdd);
                               route.addMediaLocationToDB(imgName);
                               route.setOwner(mAuth.getCurrentUser().getDisplayName()+","+mAuth.getCurrentUser().getUid());

                               Toast.makeText(LetsWalkActivity.this,"Saving path", Toast.LENGTH_LONG).show();
                               dataRef.setValue(route.getDatabaseRoute());
                               share(userInput);
                           } else {
                               Toast.makeText(LetsWalkActivity.this,"There is a route already saved as that name, please try again.", Toast.LENGTH_LONG).show();
                           }
                       }

                       @Override
                       public void onCancelled(DatabaseError databaseError) {

                       }
                   });

                } else {
                    //We are retrieving from the database
                    Query routes = dataRef;
                    routes.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (!dataSnapshot.hasChildren()) {
                                Toast.makeText(LetsWalkActivity.this,"There is no route saved under that name", Toast.LENGTH_LONG).show();
                            } else {
                                mMap.clear();
                                toAdd = new PolylineOptions();
                                polylineOptions = new PolylineOptions();
                                stop = true;
                                for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {

                                    if (check) {
                                        retrievedMedia = singleSnapshot.getValue(String.class);
                                        check = false;
                                    } else if(info){
                                        retrievedUser = singleSnapshot.getValue(String.class);
                                        info = false;
                                    } else {
                                        retrievedRoute = singleSnapshot.getValue(String.class);
                                    }
                                }
                                setUser(retrievedUser);
                                setMedia(retrievedMedia);
                                setPolylineOptions(retrievedRoute);
                                original = false;
                                share(userInput);
                        }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Toast.makeText(LetsWalkActivity.this,"Not found", Toast.LENGTH_LONG).show();
                        }
                    });
                    //This would be called before the database would find and assign the route to our retrieved variable
                    // setPolylinOptions(retrieved);
                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void setUser(String user) {
        userInfo = user.split(",");
    }

    private void share(final String pathName) {
        Log.d(TAG, "Enter share function");
        AlertDialog.Builder builder = new AlertDialog.Builder(LetsWalkActivity.this);
        builder.setTitle("If you would like to share this route, please enter your friends email address ");

        // Set up the input
        final EditText input = new EditText(LetsWalkActivity.this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS | InputType.TYPE_TEXT_VARIATION_NORMAL);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("Share", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String emailToShare = removeFullStop(input.getText().toString());
                //First get name and uid
                database = FirebaseDatabase.getInstance();
                DatabaseReference getInfo = database.getReference("users");
                Query check = getInfo;
                /*
                //Enter into database emailadress : uid
                    database = FirebaseDatabase.getInstance();
                    final DatabaseReference routeRef = database.getReference("users");
                    String email = removeFullStop(emailText.getText().toString());
                    routeRef.child(email).setValue(nameText.getText().toString()+","+user.getUid());
                 */
                check.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                       Log.d(TAG, removeFullStop("SHARED SUCCESSFULLY"));
                       boolean exists = false;
                       for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                       if(singleSnapshot.getKey().equals(emailToShare)) {
                           exists = true;
                           Log.d(TAG, "RIGHT nameUID Email: "+singleSnapshot.getKey());
                           Log.d(TAG, "RIGHT nameUID: "+nameUID);
                           nameUID = singleSnapshot.getValue(String.class);

                           String[] toShareData = nameUID.split(",");
                           //data[0] = name
                           //data[1] = uid
                           DatabaseReference shareRef =  database.getReference("Route Data").child(toShareData[0]).child(toShareData[1]).child(mAuth.getCurrentUser().getDisplayName()+pathName);
                           toAdd = polylineOptions;
                            //Populate database objects
                           route = new Route(toAdd);
                           if(original) {
                               route.setOwner(mAuth.getCurrentUser().getDisplayName()+","+mAuth.getCurrentUser().getUid());
                           } else {
                               route.setOwner(userInfo[0]+","+userInfo[1]);
                           }
                           route.addMediaLocationToDB(imgName);
                           shareRef.setValue(route.getDatabaseRoute());
                           } else {
                                Log.d(TAG, "WRONG nameUID Email: "+singleSnapshot.getKey());
                                Log.d(TAG, "WRONG nameUID: "+nameUID);
                            }

                            }
                            if(!exists) {
                                Toast.makeText(LetsWalkActivity.this,"There is no user with that email address", Toast.LENGTH_LONG).show();
                            }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }
        });
        builder.setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private static String removeFullStop(String input) {
        String regx = ".#$[]";
        char[] ca = regx.toCharArray();
        for (char c : ca) {
            input = input.replace("" + c, "");
        }

        return input;
    }

    private void retrieveRoute() {

        getInput(false);
    }

    private void saveRoute() {

        getInput(true);

    }

    private void setMedia(String retrievedMedia) {

        String[] splitData = retrievedMedia.split("\\|");
        String[] values;
        //we need some kind of check to say we have received data
        if (retrievedMedia.length() > 0) {
            for (int i = 0; i < splitData.length; i++) {
                values = splitData[i].split(",");
                /*
                values[0] = name
                values[1] = latitude
                values[2] = longitude
                 */


                MarkerOptions photoMarker = new MarkerOptions();
                photoMarker.position(new LatLng(Double.parseDouble(values[1]), Double.parseDouble(values[2])));
                photoMarker.title(values[0]);
                photoMarker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
                Marker marker = mMap.addMarker(photoMarker);
                marker.setTag(userInfo[0]+","+userInfo[1]+","+values[0]);
                Log.d(TAG, "values = "+userInfo[0]+", "+userInfo[1]+","+values[0]);
                imageMarkers.add(marker);
                mediaLocation = mediaLocation+values[0]+","+values[1]+","+values[2]+"|";
                imgName  = imgName.concat(mediaLocation);

                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        if(imageMarkers.contains(marker)) {
                            String ref = (String) marker.getTag();
                            String[] values = ref.split(",");
                            StorageReference retrieveRef = mStorageRef.child("images").child(values[0]).child(values[1]).child(values[2]+".jpg");
                            Log.d(TAG, "retrieveRef: "+retrieveRef.toString());

                            photoFile = null;
                            try {
                                photoFile = createImageFile();
                            } catch(IOException e) {
                                //Error while creating file
                            }
                            //Continue only if the file was successfully created
                            if(photoFile != null) {
                                retrieveRef.getFile(photoFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                        Intent i = new Intent();
                                        i.setAction(Intent.ACTION_VIEW);
                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                        i.setDataAndType(FileProvider.getUriForFile(LetsWalkActivity.this, "com.jamshid.letswalk", photoFile), "image/jpg");
                                        startActivity(i);
                                    }
                                });
                            }

                        }
                        return true;
                    }
                });
            }
        }

    }

    private void setPolylineOptions(String retrieved) {
        String[] splitData = retrieved.split("\\|");
        String[] values;
        //we need some kind of check to say we have received data
        if (retrieved.length() > 0) {
            for (int i = 0; i < splitData.length; i++) {
                values = splitData[i].split(",");
                retrievedFromDB.add(
                        new LatLng(Double.parseDouble(values[0]),
                                Double.parseDouble(values[1])));
            }
        }
        MarkerOptions init = new MarkerOptions();
        MarkerOptions markerOptions = new MarkerOptions();
        values = splitData[0].split(",");
        markerOptions.position(new LatLng(Double.parseDouble(values[0]), Double.parseDouble(values[1])));
        markerOptions.title("Start Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mMap.addMarker(markerOptions);
        mMap.addPolyline(retrievedFromDB);
        polylineOptions = retrievedFromDB;
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, LetsWalkActivity.this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Called called when there was an error connecting the client to the service.
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Permission was granted.
                    if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            //You can add here other case statements according to your requirement.
        }
    }

}
