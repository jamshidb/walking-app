package com.jamshid.letswalk;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Jamshid on 10/04/2017.
 */

public class DatabaseRoute {

    String routeData = "";
    String media = "";
    String originalOwner= "";

    public DatabaseRoute() {/*Default constructor required for calls to DataSnapshop.get*/}

    public DatabaseRoute(LatLng initial) {writeNewPoint(initial);}

    public void writeNewPoint(LatLng toAdd) {
        this.routeData = this.routeData.concat(toAdd.latitude+","+toAdd.longitude+"|");
    }

    public void addMedia(String toAdd) {
        this.media = toAdd;
    }

    public void setData(String routeData) {this.routeData = routeData;}

    public void setOriginalOwner(String originalOwner) {this.originalOwner = originalOwner;}

}
