package com.jamshid.letswalk;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

/**
 * Created by Jamshid on 10/04/2017.
 */

public class Route {
    DatabaseRoute dbRoute;
    PolylineOptions polylineOptions;

    public Route(PolylineOptions polylineOptions) {
        this.polylineOptions = polylineOptions;
        dbRoute = new DatabaseRoute();
        setDbRoute();
    }

    public void addPoint(LatLng toAdd) {
        polylineOptions.add(toAdd);
        addPointToDB(toAdd);
    }

    private void addPointToDB(LatLng toAdd) {
        dbRoute.writeNewPoint(toAdd);
    }

    public void addMediaLocationToDB(String toAdd) {
        dbRoute.addMedia(toAdd);
    }

    private void setDbRoute() {
        List<LatLng> points = polylineOptions.getPoints();
        for(LatLng toAdd : points) {
            addPointToDB(toAdd);
        }
    }

    public DatabaseRoute getDatabaseRoute() {
        return dbRoute;
    }

    public void setOwner(String owner) {dbRoute.setOriginalOwner(owner);}

 }
