package com.jamshid.letswalk;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Jamshid on 11/05/2017.
 */

public class SignUpActivity extends AppCompatActivity {
    private static final String TAG = "SignUpActivity";
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;

    EditText nameText;
    EditText emailText;
    EditText passText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mAuth = FirebaseAuth.getInstance();

        nameText = (EditText) findViewById(R.id.name);
        emailText = (EditText) findViewById(R.id.email);
        passText = (EditText) findViewById(R.id.pass);

        Button signUp = (Button) findViewById(R.id.btn_signup);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUser();
            }
        });

        TextView signIn = (TextView) findViewById(R.id.link_login);
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void createUser() {
        mAuth.createUserWithEmailAndPassword(emailText.getText().toString(), passText.getText().toString()).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    //Create user was successful
                    Log.d(TAG, "Create user was successful");
                    FirebaseUser user = mAuth.getCurrentUser();
                    user.sendEmailVerification();
                    UserProfileChangeRequest addName = new UserProfileChangeRequest.Builder().setDisplayName(nameText.getText().toString()).build();
                    user.updateProfile(addName);
                    Toast.makeText(SignUpActivity.this, "Signed up successfully", Toast.LENGTH_LONG).show();

                    //Enter into database emailadress : uid
                    database = FirebaseDatabase.getInstance();
                    final DatabaseReference routeRef = database.getReference("users");
                    String email = removeFullStop(emailText.getText().toString());
                    routeRef.child(email).setValue(nameText.getText().toString()+","+user.getUid());

                    finish();
                } else {
                    Log.d(TAG, "Create user was unsuccessful", task.getException());
                    Toast.makeText(SignUpActivity.this, "Sign up failed", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private String removeFullStop(String input) {
        String regx = ".#$[]";
        char[] ca = regx.toCharArray();
        for (char c : ca) {
            input = input.replace("" + c, "");
        }

        return input;
    }

}