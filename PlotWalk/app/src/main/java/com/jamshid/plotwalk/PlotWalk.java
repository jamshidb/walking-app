package com.jamshid.plotwalk;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;


public class PlotWalk extends FragmentActivity {

    private GoogleMap mMap;
    private List<LatLng> listLatLng;
    /*
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Marker mCurrLocationMarker;
    private LocationRequest mLocationRequest;
*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plot_walk);

        listLatLng = new ArrayList<LatLng>();

        try {
            GpsTracker gps = new GpsTracker(PlotWalk.this);

            //check if GPS is enabled
            if(gps.CanGetLocation()) {
                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();

                // \n is for new line
                Toast.makeText(getApplicationContext(), "Your location is - \nLat: "+latitude+"\nLong: "+longitude, Toast.LENGTH_LONG).show();
            } else {
                //Can't get location
                //GPS or Network is not enabled
                //Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }

            //Loading map
            initializeMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initializeMap() {
        if (mMap == null) {
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).get();

            //Check if map is created successfully or not
            if (mMap == null) {
                Toast.makeText(getApplicationContext(),"Sorry!\nThere was a problem loading the map", Toast.LENGTH_LONG).show();
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        initializeMap();
    }

    public void onLocationChanged(Location arg0) {
        // TODO Auto-generated method stub

    }


    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }


    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }


    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }


    public void onConnectionFailed(ConnectionResult arg0) {
        // TODO Auto-generated method stub

    }


    public void onConnected(Bundle arg0) {
        // TODO Auto-generated method stub

    }


    public void onDisconnected() {
        GpsTracker gps = new GpsTracker(PlotWalk.this);
        gps.stopUsingGPS();

    }

}