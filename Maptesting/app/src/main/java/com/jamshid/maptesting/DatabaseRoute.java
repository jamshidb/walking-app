package com.jamshid.maptesting;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Created by Jamshid on 10/04/2017.
 */

public class DatabaseRoute {

    String routeData = "";

    public DatabaseRoute() {/*Default constructor required for calls to DataSnapshop.get*/}

    public DatabaseRoute(LatLng initial) {writeNewPoint(initial);}

    public void writeNewPoint(LatLng toAdd) {
        this.routeData = this.routeData.concat(toAdd.latitude+","+toAdd.longitude+"|");
    }

    public void setData(String routeData) {this.routeData = routeData;}

}
